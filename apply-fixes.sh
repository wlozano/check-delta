#!/bin/sh

BAK=$PWD/utils.bak
REPLACEMENT="busybox libarchive-tools"
TOOLS="cpio cmp diff diff3 find xargs grep gzip sed tar"
PACKAGES="cpio-gplv2 diffutils-gplv2 findutils-gplv2 grep-gplv2 gzip-gplv2 sed-gplv2 tar-gplv2"

prepare() {
    for r in $REPLACEMENT; do
        apt install $r
    done
}

backup() {
    if [ ! -d $BAK } ; then
        mkdir $BAK
    fi
    for t in $TOOLS; do
        echo Backup $t
        if [ -e $BAK/$t ] ; then
            echo $t already present, skipping it.
            continue
        fi
        cp /bin/$t $BAK
        #rm /bin/$t
    done
}

replace_old() {
    echo Replacing tools with alternatives

    echo 'busybox cpio $@' > /bin/cpio
    echo 'bsdcpio $@' > /bin/cpio

    echo 'busybox diff %@' > /bin/diff

    echo 'busybox find $@' > /bin/find

    echo 'busybox grep $@' > /bin/grep

    echo 'busybox gzip' > /bin/gzip

    echo 'busybox sed $@' > /bin/sed

    #echo 'ARGS=`echo $@ | sed "s/--warning=no-timestamp//g"`; busybox tar $ARGS' > /bin/tar
    echo 'ARGS=`echo $@ | sed "s/--warning=no-timestamp//g"`; bsdtar $ARGS' > /bin/tar

    echo 'busybox xargs $@' > /bin/xargs

}

replace() {
    echo Replacing tools with alternatives

    echo 'busybox cpio $@' > /bin/cpio
    echo 'bsdcpio $@' > /bin/cpio

    ln -sf /bin/busybox /bin/diff

    #ln -sf /bin/busybox /bin/find

    #ln -sf /bin/ugrep /bin/grep
    ln -sf /bin/busybox /bin/grep

    ln -sf /bin/busybox /bin/gzip

    ln -sf /bin/busybox /bin/sed

    #echo 'ARGS=`echo $@ | sed "s/--warning=no-timestamp//g"`; busybox tar $ARGS' > /bin/tar
    echo 'ARGS=`echo $@ | sed "s/--warning=no-timestamp//g"`; bsdtar $ARGS' > /bin/tar

    #ln -sf /bin/busybox /bin/xargs
}

restore() {
    echo Restoring tools from backup

    for t in $TOOLS; do
        echo Restoring $t
        rm /bin/$t
        cp $BAK/$t /bin
    done
}

restore_full() {
    echo Restoring tools from backup

    for p in $PACKAGES; do
        apt install $p
    done
}

reinstall() {
    apt list --installed | cut -d'/' -f1 > pkgs
    for p in $PACKAGES ; do
        grep -v $p pkgs > pkgs.tmp
        mv pkgs.tmp pkgs
    done
    for p in `cat pkgs` ; do
        apt reinstall $p
    done
}

if [ "$1" = "backup" ] ; then
    backup
elif [ "$1" = "replace" ] ; then
    replace
elif [ "$1" = "restore" ] ; then
    restore
elif [ "$1" = "restore-full" ] ; then
    restore_full
elif [ "$1" = "reinstall" ] ; then
    reinstall
else
    echo Invalid argument
fi
