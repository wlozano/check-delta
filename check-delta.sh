UPDATE_PACKAGE_NAMES=0
UPDATE=0
UPDATE_ONLY=0
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
OUT_FILE=$SCRIPT_DIR/apertis_delta.txt
OUT_FILE_BRIEF=$SCRIPT_DIR/apertis_delta_brief.txt
OUT_FILE_TABLE=$SCRIPT_DIR/apertis_delta_table.txt
OUT_FILE_TABLE_WEB=$SCRIPT_DIR/apertis_delta_table_web.txt
COMMENT_FILE=$SCRIPT_DIR/apertis_delta_comments.txt
CATEGORY_SHOW="DF0 DF1 OP AP0 AP1 XXX"
rm $OUT_FILE
rm $OUT_FILE_BRIEF
rm $OUT_FILE_TABLE

function check_branch {
    RESULT=0
    git branch -r | grep -q $1
    if [ $? -eq 0 ] ; then
        RESULT=1
    fi
}

function get_packages {
    echo $OUT_FILE
    rm -f $OUT_FILE
    for COMPONENT in $COMPONENTS ; do
        SOURCE_URL=$BASE/$DISTRO/dists/$VERSION/$COMPONENT/$SOURCE
        OUT_COMPONENT_FILE=repo-$VERSION-$COMPONENT-packages
        rm $OUT_COMPONENT_FILE
        echo "wget $SOURCE_URL -o $OUT_COMPONENT_FILE.gz"
        wget $SOURCE_URL -O $OUT_COMPONENT_FILE.gz
        gunzip $OUT_COMPONENT_FILE.gz
        cat $OUT_COMPONENT_FILE >> $OUT_FILE
    done
}

function get_apertis_packages {
    BASE=https://repositories.apertis.org
    DISTRO=apertis
    VERSION=v2022dev2
    COMPONENTS="target"
    SOURCE=source/Sources.gz
    OUT_FILE=repo-$VERSION-packages
    get_packages
}

function get_apertis_v2022dev2_packages {
    VERSION=v2022dev2
    get_apertis_packages
}

function get_debian_packages {
    BASE=https://deb.debian.org
    DISTRO=debian
    COMPONENTS="main"
    SOURCE=source/Sources.gz
    OUT_FILE=repo-$VERSION-packages
    get_packages
}

function get_debian_buster_packages {
    VERSION=buster
    get_debian_packages
}

function get_debian_bullseye_packages {
    VERSION=bullseye
    get_debian_packages
}

function process_source {
    grep-dctrl -FPackage \* -n -w -sPackage $SCRIPT_DIR/repo-$1-packages > $SCRIPT_DIR/repo-$1-packages-brief
}

function get_source_package_name {
    PKGNAME=`basename $PWD`
    if [ "$PKGNAME" == "gnome-settings-daemon-data" ] ; then
        PKGNAME=gnome-settings-daemon
    fi
    if [ "$PKGNAME" == "gtk-2.0" ] ; then
        PKGNAME=gtk+2.0
    fi
    if [ "$PKGNAME" == "gtk-3.0" ] ; then
        PKGNAME=gtk+3.0
    fi
    echo $PKGNAME
}

function check_debian_full {
    #PKG=`basename $PWD`
    PKG=`get_source_package_name`
    RESULT=0
    grep-dctrl -FPackage $PKG -w $SCRIPT_DIR/repo-$1-packages > /dev/null
    if [ $? -eq 0 ] ; then
        RESULT=1
    fi
}

function check_debian {
    #PKG=`basename $PWD`
    PKG=`get_source_package_name`
    RESULT=0
    grep -q ^$PKG$ -w $SCRIPT_DIR/repo-$1-packages-brief
    if [ $? -eq 0 ] ; then
        RESULT=1
    fi
}

function diff_branches_filenames {
    RESULT=`git diff $1 $2 --name-only ':(exclude)debian/apertis/' ':(exclude)debian/changelog'`
}

function diff_branches_nfilenames {
    RESULT=`git diff $1 $2 --name-only ':(exclude)debian/apertis/'  ':(exclude)debian/changelog' | wc --lines`
}

function diff_branches_license_old {
    RESULT=0
    git diff --unified=0 $1 $2 ':(exclude)debian/apertis/' | egrep -qi "licen|GPL|BSD"
    if [ $? -eq 0 ] ; then
        RESULT=1
    fi
}
    
function diff_branches_license {
    RESULT=`git diff --unified=0 $1 $2 ':(exclude)debian/apertis/' | egrep -i "licen|GPL|BSD" | egrep -v "Refresh the automatically detected licensing information"`
}

function diff_branches_debian_control {
    RESULT=0
    git diff --quiet $1 $2 debian/control
    if [ $? -ne 0 ] ; then
        RESULT=1
    fi
}

function diff_branches_debian_control_deps {
    RESULT=`git diff --unified=100 $1 $2 debian/control | awk '{if ($0 ~ "Package:") { exit } else { print $0 } }' | grep ^"-" | cut -c2- | grep -v ^"Source:" | grep -v ^"--" | grep -v ^"#" | sed 's/^\s*//g' | sed 's/(.*)//g' | sed 's/<.*>//g' | sed 's/\[.*\]//g' | sed 's/Build-Depends://g' | sed 's/Depends://g' | tr ',' ' ' | tr -d '\n' | sed 's/^\s*//g' | sed 's/\s\s\+/ /g'`
    RESULT=$RESULT
}

function diff_branches_debian_control_deps_diff {
    DROPPED=`git diff --unified=100 $1 $2 debian/control | awk '{if ($0 ~ "Package:") { exit } else { print $0 } }' | grep ^"-" | cut -c2- | grep -v ^"Source:" | grep -v ^"--" | grep -v ^"#" | sed 's/^\s*//g' | sed 's/(.*)//g' | sed 's/<.*>//g' | sed 's/\[.*\]//g' | sed 's/Build-Depends://g' | sed 's/Depends://g' | tr ',' ' ' | tr -d '\n' | sed 's/^\s*//g' | sed 's/\s\s\+/ /g'`
    ADDED=`git diff --unified=100 $1 $2 debian/control | awk '{if ($0 ~ "Package:") { exit } else { print $0 } }' | grep ^"+" | cut -c2- | grep -v ^"Source:" | grep -v ^"++" | grep -v ^"#" | sed 's/^\s*//g' | sed 's/(.*)//g' | sed 's/<.*>//g' | sed 's/\[.*\]//g' | sed 's/Build-Depends://g' | sed 's/Depends://g' | tr ',' ' ' | tr -d '\n' | sed 's/^\s*//g' | sed 's/\s\s\+/ /g'`
    RESULT=
    for PKG_CHECK in $DROPPED ; do
        echo $ADDED | grep -w -q "$PKG_CHECK"
        if [ $? -ne 0 ] ; then
            RESULT="$RESULT $PKG_CHECK"
        fi
    done
    RESULT=$RESULT
}

function get_comment {
    RESULT=`grep ^$1 $2 | cut -f2-`
}

function run_diffs {
    diff_branches_filenames $1 $2
    DIFFS_FILENAMES=$RESULT
    diff_branches_nfilenames $1 $2
    DIFFS_NFILENAMES=$RESULT
    diff_branches_license $1 $2
    DIFFS_LICENSE=$RESULT
    diff_branches_debian_control $1 $2
    DIFFS_CONTROL=$RESULT     
    diff_branches_debian_control_deps $1 $2
    DIFFS_CONTROL_DEPS=$RESULT
    diff_branches_debian_control_deps_diff $1 $2
    DIFFS_CONTROL_DEPS_DIFF=$RESULT
}

if [ "$UPDATE_PACKAGE_NAMES" = "1" ] ; then
    get_apertis_v2022dev2_packages
    get_debian_bullseye_packages
    get_debian_buster_packages
    process_source v2022dev2-target
    process_source buster-main
    process_source bullseye-main
fi

PKGS=`cat $SCRIPT_DIR/repo-v2022dev2-target-packages-brief`

printf "PKG\tBUSTER\tBULLSEYE\tV2022DEV2\tDIFFS_PRESENT\tDIFFS_NFILENAMES\tDIFFS_LICENSE_PRESENT\tDIFFS_CONTROL\tDIFFS_CONTROL_DEPS_DIFF\tCATEGORY\tINFO\tOTHER\n" >> $OUT_FILE_BRIEF
printf -- "Package\tCategory\tInformation\n" >> $OUT_FILE_TABLE
printf -- "-------\t--------\t-----------\n" >> $OUT_FILE_TABLE

for PKG in $PKGS ; do
    cd ~/pkg
    echo "Procesing $PKG" 1>&2
    if [ $UPDATE = "1" ] ; then
        if [ ! -d ~/pkg/$PKG ] ; then
            cd ~/pkg/
            git clone --depth 1 --no-single-branch  git@gitlab.apertis.org:pkg/$PKG.git
        fi
        cd ~/pkg/$PKG
        git fetch -p
        git checkout apertis/v2022dev2
        git rebase
    elif [ $UPDATE = "2" ] ; then
        cd ~/pkg/$PKG
        git checkout apertis/v2022dev2
        if [ $? -ne 0 ] ; then
            echo WARNING ON $PKG
        fi
        git rebase
        if [ $? -ne 0 ] ; then
            echo WARNING ON $PKG
        fi
    elif [ $UPDATE = "3" ] ; then
        if [ ! -d ~/pkg/$PKG ] ; then
            cd ~/pkg/
            git clone --depth 1 --no-single-branch  git@gitlab.apertis.org:pkg/$PKG.git
        fi
    fi

    if [ "$UPDATE_ONLY" = "1" ] ; then
        continue
    fi

    if [ "$PKG" = "gtk+2.0" ] ; then PKG="gtk-2.0"; fi
    if [ "$PKG" = "gtk+3.0" ] ; then PKG="gtk-3.0"; fi

    cd ~/pkg/$PKG

    if false ; then
        check_branch "debian/buster"
        BUSTER=$RESULT
        check_branch "debian/bullseye"
        BULLSEYE=$RESULT
    fi

    check_debian buster-main
    BUSTER=$RESULT
    check_debian bullseye-main
    BULLSEYE=$RESULT

    check_branch "apertis/v2022dev2"
    V2022DEV2=$RESULT

    #echo $PKG >> $OUT_FILE

    DIFFS_FILENAMES=
    DIFFS_NFILENAMES=0
    DIFFS_LICENSE=0
    DIFFS_CONTROL=0
    DIFFS_CONTROL_DEPS=
    DIFFS_CONTROL_DEPS_DIFF=

    if [ "$BULLSEYE" = "1" ] && [ "$V2022DEV2" = "1" ] ; then
        run_diffs origin/debian/bullseye origin/apertis/v2022dev2 
    elif [ "$BUSTER" = "1" ] && [ "$V2022DEV2" = "1" ] ; then
        run_diffs origin/debian/buster origin/apertis/v2022dev2    
    fi

    if false  ; then
        diff_branches_debian_control origin/debian/bullseye origin/apertis/v2022dev2 
        echo RESULT $RESULT 
    fi
    
    DIFFS_PRESENT=0
    if [ "$DIFFS_FILENAMES" != "" ] ; then
        DIFFS_PRESENT=1
    fi
    
    DIFFS_LICENSE_PRESENT=0
    if [ "$DIFFS_LICENSE" != "" ] ; then
        DIFFS_LICENSE_PRESENT=1
    fi

    get_comment $PKG $COMMENT_FILE
    COMMENT="$RESULT"
    CATEGORY=`echo "$COMMENT" | cut -f1`
    INFO=`echo "$COMMENT" | cut -f2`
    OTHER=`echo "$COMMENT" | cut -f3`

    if [ "$BULLSEYE" != "1" ] || [ "$DIFFS_PRESENT" != "0" ] ; then
        printf "$PKG\t$BUSTER\t$BULLSEYE\t$V2022DEV2\t$DIFFS_PRESENT\t$DIFFS_NFILENAMES\t$DIFFS_LICENSE_PRESENT\t$DIFFS_CONTROL\t$DIFFS_CONTROL_DEPS_DIFF\t$CATEGORY\t$INFO\t$OTHER\n" >> $OUT_FILE_BRIEF
        if [ "$CATEGORY" != "" ] ; then
            echo $CATEGORY_SHOW | grep -w -q "$CATEGORY"
            if [ $? -eq 0 ] ; then
                printf "$PKG\t$CATEGORY\t$INFO\n" >> $OUT_FILE_TABLE
            fi
        fi
    fi
    if [ "$DIFFS_PRESENT" != "0" ] ; then
        echo "*** $PKG *** " >> $OUT_FILE
        echo "BUSTER: $BUSTER" >> $OUT_FILE
        echo "BULLSEYE: $BULLSEYE" >> $OUT_FILE
        echo "V2022DEV2: $V2022DEV2" >> $OUT_FILE
        echo "DIFFS_PRESENT: $DIFFS_PRESENT" >> $OUT_FILE
        echo "DIFFS_NFILENAMES: $DIFFS_NFILENAMES" >> $OUT_FILE
        echo "DIFFS_LICENSE_PRESENT: $DIFFS_LICENSE_PRESENT" >> $OUT_FILE
        echo "DIFFS_CONTROL: $DIFFS_CONTROL" >> $OUT_FILE
        #echo "DIFFS_CONTROL_DEPS: $DIFFS_CONTROL_DEPS" >> $OUT_FILE
        echo "DIFFS_CONTROL_DEPS_DIFF: $DIFFS_CONTROL_DEPS_DIFF"  >> $OUT_FILE
        echo "CATEGORY: $CATEGORY" >> $OUT_FILE
        echo "INFO: $INFO" >> $OUT_FILE
        echo "OTHER: $OTHER" >> $OUT_FILE
        echo "*** " >> $OUT_FILE
        echo "DIFFS_FILENAMES" >> $OUT_FILE
        echo "$DIFFS_FILENAMES" >> $OUT_FILE
        echo "*** " >> $OUT_FILE
        echo "DIFFS_LICENSE" >> $OUT_FILE
        echo "$DIFFS_LICENSE" >> $OUT_FILE
        echo "****************************************************************************" >> $OUT_FILE
        echo >> $OUT_FILE
    fi
done

cat  $OUT_FILE_TABLE | sed 's/\t/:|/g' | column -t -s':' >  $OUT_FILE_TABLE_WEB
